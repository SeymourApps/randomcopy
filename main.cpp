#include <windows.h>
#include <iostream>

#define AddBTN 101

HWND addBtn;
HWND addTitle;
HWND addText;
HWND toCopy;

class Copy {
public:
    char title[64];
    char text[64];

    int index;

    HWND hWnd;

    void cop();
};
void Copy::cop() {
    SetWindowText(toCopy, text);

    const size_t len = strlen(text) + 1;
    HGLOBAL hMem =  GlobalAlloc(GMEM_MOVEABLE, len);
    memcpy(GlobalLock(hMem), text, len);
    GlobalUnlock(hMem);
    OpenClipboard(0);
    EmptyClipboard();
    SetClipboardData(CF_TEXT, hMem);
    CloseClipboard();
}

WNDCLASSEX window;

HWND wHwnd;

Copy copies[10];

int y;
int prevY = 0;
int index = 0;

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    ZeroMemory(&window, sizeof(WNDCLASSEX));

    window.cbSize = sizeof(WNDCLASSEX);
    window.style = CS_HREDRAW | CS_VREDRAW;
    window.lpfnWndProc = WindowProc;
    window.hInstance = hInstance;
    window.hCursor = LoadCursor(NULL, IDC_ARROW);
    window.hbrBackground = (HBRUSH)COLOR_WINDOW;
    window.lpszClassName = "MainWindow";

    RegisterClassEx(&window);

    wHwnd = CreateWindowEx(NULL,
                          "MainWindow", "Random Copy",
                          WS_OVERLAPPEDWINDOW,
                          300, 300,
                          600, 400,
                          NULL, NULL,
                          hInstance, NULL);
    ShowWindow(wHwnd, nCmdShow);

    MSG msg;

    while(GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch(message) {
        case WM_CREATE: {
            addBtn = CreateWindow("button", "Add",
                                  WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                  475, 300,
                                  100, 50,
                                  hWnd, (HMENU)AddBTN, NULL, NULL);
            addText = CreateWindow("edit", "Text",
                                   WS_VISIBLE | WS_CHILD | WS_BORDER,
                                   425, 240,
                                   150, 50,
                                   hWnd, NULL, NULL, NULL);
            addTitle = CreateWindow("edit", "Title",
                                    WS_VISIBLE | WS_CHILD | WS_BORDER,
                                    425, 205,
                                    150, 25,
                                    hWnd, NULL, NULL, NULL);
            toCopy = CreateWindow("edit", "<Your Text Here>",
                                  WS_VISIBLE | WS_CHILD | WS_BORDER,
                                  10, 300,
                                  380, 30,
                                  hWnd, NULL, NULL, NULL);
        } break;
        case WM_COMMAND: {
            switch(LOWORD(wParam)) {
                case AddBTN: {
                    if (index > 0) {
                        y = prevY + 40;
                    } else {
                        y = prevY + 10;
                    }

                    GetWindowText(addText, copies[index].text, 64);
                    GetWindowText(addTitle, copies[index].title, 64);

                    SetWindowText(addText, "");
                    SetWindowText(addTitle, "");

                    copies[index].hWnd = CreateWindow("button", copies[index].title,
                                               WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                               10, y,
                                               380, 30,
                                               hWnd, (HMENU)(index + 200), NULL, NULL);

                    prevY = y;
                    ++index;
                } break;

                default: {
                    for (int i = 200; i < 210; i++) {
                        if (LOWORD(wParam) == i) {
                            copies[i - 200].cop();
                        }
                    }
                } break;
            }
        } break;
        case WM_DESTROY: {
            PostQuitMessage(0);
            return 0;
        } break;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}
